from urllib.parse import quote as quoteurl

CLIPURL = "http://clip.med.yale.edu/shm/distribution"
CLIPFILES = [
    "Mutability.csv",   # Space-separated per-motif mutability model values
    "Substitution.csv", # Space-separated per-motif substitution model values
    "SHM ver 0.1.zip",  # R source code
    # save()'d R objects (NOT source code or RDS format)
    "FiveMersR.RData",  # "FiveMersR": list of 193 per-motif matrices
    "FIVE_S.R",         # "FIVE": matrix of 4x1024 per-motif counts
    # Equivalent to copies in the zip, just different line endings and/or
    # source() of common file
    "mutability.R",
    "substitution.R"]

rule all:
    input: ["fig1A.svg", "fig3B.svg", "fig4A.svg", "s5f_baseline.csv", "s5f.csv", "yale_shm_dist/SHM ver 0.1/mutability.R"]

rule clean:
    shell: "rm -f fig1A.svg fig3B.svg fig4A.svg"

rule realclean:
    shell: "rm -f fig1A.svg fig3B.svg fig4A.svg s5f.csv s5f_baseline.csv"

rule f4A:
    output: "fig4A.svg"
    input: "s5f.csv"
    script: "fig4A.R"

rule fig3B:
    output: "fig3B.svg"
    input: "s5f.csv"
    script: "fig3B.R"

rule fig1A:
    output: "fig1A.svg"
    input: "s5f.csv"
    script: "fig1A.R"

# Combined S5F CSV file, from the data in the paper
rule s5f_vals:
    output: "s5f.csv"
    input:
        mut="yale_shm_dist/Mutability.csv",
        sub="yale_shm_dist/Substitution.csv"
    script: "parse_s5f.R"

# Alternate version derived from BASELINe, before I knew where the paper's data
# lived
rule s5f_vals_baseline:
    output: "s5f_baseline.csv"
    script: "get_s5f_baseline.R"

# Substitution and Mutability files are identical to canonical Yale version (as
# with the BASELINe version, from before I knew where to get the right files)
rule armadillo_files:
    output:
        mut=temp("ARMADiLLO/Mutability.csv"),
        sub=temp("ARMADiLLO/Substitution.csv"),
        css=temp("ARMADiLLO/AMA.css"),
        bin=temp("ARMADiLLO/batch_analyze_mutations_from_SMUA"),
        png=temp("ARMADiLLO/unusual.png"),
    input: "ARMADiLLO_MAC_OSX.zip"
    shell: "unzip {input}"

rule armadillo_zip:
    output: temp("ARMADiLLO_MAC_OSX.zip")
    shell: "curl 'https://sites.duke.edu/armadillo/files/2018/03/ARMADiLLO_MAC_OSX.zip' > {output}"

rule yale_shm_dist_unzip:
    output: expand("yale_shm_dist/SHM ver 0.1/{thing}", thing=["Baseline_Functions.r", "mutability.R", "substitution.R"])
    input: "yale_shm_dist/SHM ver 0.1.zip"
    shell: "unzip '{input}' -d yale_shm_dist"

rule yale_shm_dist:
    output: [temp(x) if ".zip" in x else x for x in expand("yale_shm_dist/{fname}", fname=CLIPFILES)]
    run:
        for fname, path in zip(CLIPFILES, output):
            url = CLIPURL + "/" + quoteurl(fname)
            shell(f"curl '{url}' > '{path}'")
