# Data Gathering from Yaari 2013

Some scripts to aggregate results and metadata from:

Yaari G, Vander Heiden JA, Uduman M, Gadala-Maria D, Gupta N, Stern JNH, O’Connor KC, Hafler DA, Laserson U, Vigneault F and Kleinstein SH (2013) Models of somatic hypermutation targeting and substitution based on synonymous mutations from high-throughput immunoglobulin sequencing data. Front. Immunol.  4:358. doi: [10.3389/fimmu.2013.00358](https://doi.org/10.3389/fimmu.2013.00358)

Combined mutability+substitution [S5F CSV file](s5f.csv) is generated from
separate [Mutability.csv] and [Substitution.csv] files from
<http://clip.med.yale.edu/shm/download.php>.  (Note that the URL in the paper
doesn't work at the moment because it ends in `/SHM` rather than `/shm`.)

(Before the authors helped me get access to the data and code files I found
some related files elswhere.  Another combined mutability+substitution
[S5F CSV file](s5f_baseline.csv) is generated from RData I found here:
<http://selection.med.yale.edu/baseline/Archive/> These numbers look related
but not identical to the paper's.  In particular mutability values here are a
small fraction of those from the paper.)

Usage:

    git clone https://gitlab.com/ressy-group/paper-helpers/paper-helper-yaari-2013.git
    cd paper-helper-yaari-2013/
    conda env update --file environment.yml
    conda activate paper-helper-yaari-2013
    snakemake -j 1 realclean
    snakemake -j 1

Glossary:

 * **Substitution model**: covers the relative likelihood of a mutation to one
   base over another for each motif (for positions where all possible mutations
   would be synonymous)
 * **Targeting model**: covers the relative likelihood of a mutation for one
   motif over another
 * **B<sub>M</sub>**: Background frequency (B) for mutation opportunities from a particular
   motif (M); weighted sum across specific per-base mutation likelihoods for
   all synonymous mutation cases
 * **C<sub>M</sub>**: Observed instances of mutation (C?) for a particular motif (M)
 * **m<sub>M</sub>**: mutability score; ratio of C<sub>M</sub>/B<sub>M</sub>
 * **GL**: Germline
 * **OS**: Observed Sequence
 * **i**: index of position within sequence
 * **j**: index of sequence

## Checks

As a quick check I recreated some figure panels from this S5F CSV file.

1A  shows the substitution likelihoods for various motifs centered on G.  It's
not quite identical to the
[original](https://www.frontiersin.org/files/Articles/64394/fimmu-04-00358-HTML/image_m/fimmu-04-00358-g001.jpg),
but close.

![Fig 1A Recreation](fig1A.svg)

3B shows mutability per motif for each center NT per motif
([original](https://www.frontiersin.org/files/Articles/64394/fimmu-04-00358-HTML/image_m/fimmu-04-00358-g003.jpg)).

![Fig 3B Recreation](fig3B.svg)

4A shows distributions of observed mutability values grouped by hot/cold spot.
These look equivalent to the
[paper's version](https://www.frontiersin.org/files/Articles/64394/fimmu-04-00358-HTML/image_m/fimmu-04-00358-g004.jpg).

![Fig 4A Recreation](fig4A.svg)

A check using a specific ratio of mutability values:

From the paper text:

> There is a 62.7-fold difference between the most mutable (GGGCA, mutability
> = 9.56) and least mutable (TGCGA, mutability = 0.15) WRC/GYW hot-spot motif. 

<!-- check_mut_range.R -->
Checking the  least and most mutable WRC/GYW motifs in the CSV file, things
mostly match:

    Motif Mutability MutSource MutLower25 MutUpper25
    TGCGA  0.1480105  Measured  0.1035892  0.2015482
    GGGCA  9.2781691  Measured  7.7463982 11.3814154

From that the ratio is also 62.7.

[Mutability.csv]: http://clip.med.yale.edu/shm/distribution/Mutability.csv
[Substitution.csv]: http://clip.med.yale.edu/shm/distribution/Substitution.csv
