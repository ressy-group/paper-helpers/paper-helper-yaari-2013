#!/usr/bin/env Rscript

library(ggplot2)
library(gridExtra)

SPOT_COLORS <- data.frame(
  Spot = c("WRC/GYW", "WA/TW", "SYC/GRS", "Neutral"),
  Color = c("#ff0000", "#229922", "#0000ff", "#AAAAAA")
)

BASE_COLORS <- data.frame(
  Base = c("T", "G", "C", "A"),
  Color = c("#ff8888", "#88ff88", "#8888ff", "#ffff88")
)

BASES <- c("T", "G", "C", "A")

parse_spot <- function(txt) {
  spots <- data.frame(
    # Each hot/cold spot checked is pair of reverse-complements of a pattern
    SpotWRCGYW = grepl("^([AT][AG]C..|..G[CT][AT])$", txt),
    SpotWATW = grepl("^(.[AT]A..|..T[AT].)$", txt),
    SpotSYCGRS = grepl("^([GC][CT]C..|..G[AG][GC])$", txt)
  )
  spots$Spot <- with(spots, ifelse(SpotWRCGYW, "WRC/GYW",
                 ifelse(SpotWATW, "WA/TW",
                        ifelse(SpotSYCGRS, "SYC/GRS", "Neutral"))))
  spots
}

# from TTTT to AAAAA and everything in between
# if fwd, order is    TTTTT, TTTTG, TTTTC, ...
# otherwise, order is TTTTT, GTTTT, CTTTT, ...
build_motifs <- function(len=5, fwd=TRUE) {
  # expand.grid makes the first vector vary fastest, but by default we want the
  # other way around, hence rev().  If we WANT the first vector varying fastest,
  # skip rev.
  #func <- if (fwd) rev else function(x) x
    bases <- c("T", "G", "C", "A")
    func <- rev
  if (! fwd) {
    func <- function(x) x # noop
    bases <- rev(bases)
  }
  do.call(paste0, func(do.call(expand.grid, rep(list(bases), len))))
}

plot_mutability <- function(s5f, motif_subset_pattern, fwd=TRUE, title="\n") {

  all_motifs <- build_motifs(len=5, fwd=fwd)
  x <- subset(s5f, grepl(gsub("N", ".", motif_subset_pattern), Motif))
  x <- x[order(match(x$Motif, all_motifs)), ]
  x$Motif <- factor(x$Motif, levels = unique(x$Motif))

  # for expliclity positioning the bars with some offset added
  offset <- 3
  x$XMIN <- as.integer(x$Motif) - 1
  x$XMAX <- as.integer(x$Motif)
  x$YMIN <- offset
  x$YMAX <- offset + x$Mutability
  
  ggplot(
    x,
    #aes(x = Motif, y = Mutability, fill = Spot)) +
    aes(xmin=XMIN, xmax=XMAX, ymin=YMIN, ymax=YMAX, fill = Spot)) +
    geom_rect(show.legend = FALSE) +
    # 5' -> 3' is center outward or outward to center?
    annotate("text", x = 0, y = 0, label = if (fwd) "5'" else "3'") +
    annotate("text", x = 160, y = max(s5f$Mutability)/3 + offset, label = if (fwd) "3'" else "5'") +
    labs(x = NULL, y = NULL) +
    # apparently can't restrict y limits without clipping with coord_polar, it seems
    ylim(0, max(s5f$Mutability) + offset) +
    coord_polar(start = pi/2) +
    scale_fill_manual(
      breaks = SPOT_COLORS$Spot, values=SPOT_COLORS$Color) +
    ggtitle(paste(title, motif_subset_pattern, sep = "\n")) +
    theme(
      axis.text = element_blank(),
      axis.ticks = element_blank(),
      plot.title = element_text(hjust = 0.5),
      panel.background = element_blank())
}

# https://stackoverflow.com/a/53160799/4499968
zoom_polar <- function(plt, ratio) {
  plot_build <- ggplot_build(plt)
  plot_build[["layout"]][["panel_params"]][[1]][["r.range"]][2] <- plot_build[["layout"]][["panel_params"]][[1]][["r.range"]][2]*ratio
  ggplot_gtable(plot_build)
}

fig3B <- function(s5f) {
  arrangeGrob(
    zoom_polar(plot_mutability(s5f, "NNANN", title="Fig 3B Re-creation\n"), 0.5),
    zoom_polar(plot_mutability(s5f, "NNTNN", FALSE), 0.5),
    zoom_polar(plot_mutability(s5f, "NNCNN"), 0.5),
    zoom_polar(plot_mutability(s5f, "NNGNN", FALSE), 0.5), nrow=1, ncol=4)
}

s5f <- read.csv("s5f.csv")
s5f <- cbind(s5f, parse_spot(s5f$Motif))

if (any(grepl("^--file", commandArgs(trailingOnly = FALSE)))) {
  pdf(NULL) # stop making Rplots.pdf!
  ggsave("fig3B.svg", fig3B(s5f), width = 12, height = 4)
}


# Extra -------------------------------------------------------------------


# make data frame of motif annotation details, defining rectangles of same-base
# stretches across motifs.  This depends on the order of the motifs given.
build_motif_annots <- function(motifs) {
  # for each position in the motif, cycle through the bases and define stretches
  # of the same base.  For the slowest-changing position this will be four rows
  # (A, C, T, G) while for the fastest-changing it will be one row per base over
  # and over.
  do.call(rbind, lapply(1:nchar(motifs[1]), function(pos) {
    bases <- substr(motifs, pos, pos)
    # what are the positions where each base does NOT match the previous base?
    # those are the breakpoints.  Defined this way, each index is the last of a
    # set, and the very end (last idx) is implicit
    idx_ends <- c(which(bases[-length(bases)] != bases[-1]))
    idx_starts <- c(1, idx_ends + 1) # start indexes, with first explicitly added
    idx_ends <- c(idx_ends, length(bases)) # explicitly add last index too
    data.frame(
      Motif = motifs[idx_starts],
      Base = bases[idx_starts],
      Pos = pos, # position in motif
      Start = idx_starts -1, # zero-indexed now, and will line up with the ends
      End = idx_ends,
      # midpoints for convenience
      PosMid = pos + 0.5,
      Mid = (idx_starts - 1 + idx_ends)/2)
    }))
  }

# motif_annots <- build_motif_annots(build_motifs(2))
# ggplot(
#   motif_annots,
#   aes(
#     fill=Base, label=Base,
#     size = 8 - Pos,
#     x=Mid, y=PosMid, xmin=Start, xmax=End, ymin=Pos, ymax=Pos+1)) +
#   geom_rect(show.legend = FALSE) +
#   geom_text(angle=90, show.legend = FALSE) +
#   scale_size_identity() +
#   scale_fill_manual(breaks = BASE_COLORS$Base, values=BASE_COLORS$Color)
