#!/usr/bin/env Rscript
# extract S5F numbers as CSV from the space-separated files from Yale

mut <- read.csv("yale_shm_dist/Mutability.csv", sep = " ")
sub <- read.csv("yale_shm_dist/Substitution.csv", sep = " ")

s5f <- data.frame(
  Motif = mut$Fivemer,
  Mutability = mut$Mutability,
  MutSource = mut$Source,
  MutLower25 = mut$lower25,
  MutUpper25 = mut$upper25)

s5f_sub <- data.frame(
  Motif = sub$Fivemer,
  SubA = sub$A,
  SubC = sub$C,
  SubG = sub$G,
  SubT = sub$T,
  SubSource = sub$Source)

if (! all(s5f$Motif %in% s5f_sub$Motif) ) {
  stop("Motif mismatch for substitution vs mutability data")
}

s5f <- cbind(
  s5f,
  subset(s5f_sub, select = -Motif)[match(s5f$Motif, s5f_sub$Motif), ])
write.csv(s5f, "s5f.csv", quote=FALSE, row.names=FALSE)
